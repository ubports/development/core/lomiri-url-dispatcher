# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-04 17:00+0000\n"
"PO-Revision-Date: 2023-01-06 18:49+0000\n"
"Last-Translator: Muhammad <muhammad23012009@hotmail.com>\n"
"Language-Team: Urdu <https://hosted.weblate.org/projects/lomiri/lomiri-url-"
"dispatcher/ur/>\n"
"Language: ur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: gui/lomiri-url-dispatcher-gui.desktop.in:3
msgid "Lomiri URL Dispatcher GUI"
msgstr ""

#: gui/lomiri-url-dispatcher-gui.desktop.in:7
msgid "Lomiri;URL;dispatcher;request;open;application;"
msgstr ""

#: data/bad-url.qml:31
msgid "Unrecognized Address"
msgstr "ناقابل شناخت پتہ"

#: data/bad-url.qml:32
#, qt-format
msgid "Cannot open “%1” addresses."
msgstr ""

#: data/bad-url.qml:35
msgid "OK"
msgstr "ٹھیک ہے"

#: gui/lomiri-url-dispatcher-gui.qml:10
msgid "URL Dispatcher GUI"
msgstr ""

#: gui/lomiri-url-dispatcher-gui.qml:32
msgid "URL (e.g. 'https://example.com')"
msgstr ""

#: gui/lomiri-url-dispatcher-gui.qml:46
msgid "Send URL"
msgstr ""

#~ msgid "Ubuntu can't open addresses of type “%1”."
#~ msgstr "اوبنٹو \"%1\" کے طرح کے اڈریس نہیں کھول سکتا۔"
