# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-url-dispatcher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-url-dispatcher\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-04 17:00+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: co\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gui/lomiri-url-dispatcher-gui.desktop.in:3
msgid "Lomiri URL Dispatcher GUI"
msgstr ""

#: gui/lomiri-url-dispatcher-gui.desktop.in:7
msgid "Lomiri;URL;dispatcher;request;open;application;"
msgstr ""

#: data/bad-url.qml:31
msgid "Unrecognized Address"
msgstr ""

#: data/bad-url.qml:32
#, qt-format
msgid "Cannot open “%1” addresses."
msgstr ""

#: data/bad-url.qml:35
msgid "OK"
msgstr ""

#: gui/lomiri-url-dispatcher-gui.qml:10
msgid "URL Dispatcher GUI"
msgstr ""

#: gui/lomiri-url-dispatcher-gui.qml:32
msgid "URL (e.g. 'https://example.com')"
msgstr ""

#: gui/lomiri-url-dispatcher-gui.qml:46
msgid "Send URL"
msgstr ""
